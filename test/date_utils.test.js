
const { DateUtils, DateFormat } = require("../src/date_utils");
const assert = require("assert");



it("shoud return correct date", () => {

    assert.equal(DateUtils.parseDate("23/11/2021").getTime(), new Date(2021, 10, 23).getTime());

});

it('shoud return 2021-11-23T23:59:59.999Z', function() {
    assert.equal(DateUtils.getMaxDate(new Date(2021, 10, 23)).toISOString(), '2021-11-23T23:59:59.999Z');
});

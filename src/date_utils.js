
class DateFormat {
    /**
     * Les mois commence de 1 à 12
     */
    static DD_MM_YYYY = "DD/MM/AAAA";
    static ISO = "ISO";
}

class DateUtils {

    /**
     * Retourne une date sans les heures, minutes et secondes
     * @param {Date?} date 
     */
    static getDate(date) {
        if (!date)
            date = new Date();
        return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }

    /**
     * 
     * @param {Date?} date 
     * @returns 
     */
    static getMaxDate(date) {
        if (!date)
            date = new Date();
        return new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59, 999);
    }

    /**
     * 
     * @param {string} date 
     * @param {DateFormat} format le format dans lequel est la date par defaut `DateFormat.DD_MM_YYYY`
     * @returns 
     */
     static parseDate(date, format) {
        if (!date)
            return null;

        if (!format)
            format = DateFormat.DD_MM_YYYY;

        if(format == DateFormat.ISO)
            return new Date(date);

        const parts = date.split("/");

        if (parts.length != 3)
            return null;

        for (let i=0; i<3; ++i) {
            const value = parseInt(parts[i]);
            if (isNaN(value))
                return null;
            parts[i] = value;
        }

        return new Date(
            parts[2],
            parts[1] - 1,
            parts[0]
        );
    }

    /**
     * 
     * @param {Date|string|number} date 
     */
    static toDate(date) {
        return new Date(date);
    }

    /**
     * 
     * @param {Date} date 
     * @param {{ years?: number, months?: number, days?: number, months?: months }} params 
     */
    static add(date, params) {

    }

    /**
     * 
     * @param {Date|string|number} date 
     */
    static startOfDay(date) {
        const d = this.toDate(date);
        d.setHours(0, 0, 0, 0);
        return d;
    }

    /**
     * 
     * @param {Date|string|number} date 
     */
    static endOfDay(date) {
        const d = this.toDate(date);
        d.setHours(23, 59, 59, 999);
        return d;
    }

    /**
     * 
     * @param {Date|string|number} date 
     * @param {number} weekStartsOn Le jour de debut de la semaine 1 pour le lundi
     * @returns 
     */
    static startOfWeek(date, weekStartsOn = 1) {
        const d = this.toDate(date);
        const day = d.getDay();
        const diff = (day < weekStartsOn ? 7 : 0) + day - weekStartsOn;
        d.setDate(d.getDate() - diff);
        d.setHours(0, 0, 0, 0);
        return d;
    }

    /**
     * 
     * @param {Date|string|number} date 
     * @param {number} weekStartsOn Le jour de debut de la semaine 1 pour le lundi
     * @returns 
     */
    static endOfWeek(date, weekStartsOn = 1) {
        const d = this.toDate(date);
        const day = d.getDay();
        const diff = (day < weekStartsOn ? -7 : 0) + 6 - (day - weekStartsOn);
        d.setDate(d.getDate() + diff);
        d.setHours(23, 59, 59, 999);
        return d;
    }

    /**
     * 
     * @param {Date|string|number} date 
     * @returns 
     */
    static startOfMonth(date) {
        const d = this.toDate(date);
        d.setDate(1);
        d.setHours(0, 0, 0, 0);
        return d;
    }

    /**
     * 
     * @param {Date|string|number} date 
     * @returns 
     */
    static endOfMonth(date) {
        const d = this.toDate(date);
        d.setFullYear(date.getFullYear(), date.getMonth() + 1, 0);
        d.setHours(23, 59, 59, 999);
        return d;
    }

    /**
     * 
     * @param {Date|string|number} date 
     * @returns 
     */
    static startOfYear(date) {
        const d = this.toDate(date);
        d.setFullYear(date.getFullYear(), 0, 1);
        d.setHours(0, 0, 0, 0);
        return d;
    }

    /**
     * 
     * @param {Date|string|number} date 
     * @returns 
     */
    static endOfYear(date) {
        const d = this.toDate(date);
        d.setFullYear(date.getFullYear(), 12, 0);
        d.setHours(23, 59, 59, 999);
        return d;
    }

}

module.exports = {
    DateFormat,
    DateUtils
};
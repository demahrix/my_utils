

class Utils {

    static _emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

    /**
     * Vérifie que `value` à la forme d'une adresse email
     * @param {String} value
     * @returns {boolean}
     */
    static isEmail(value) {
        return Utils._emailRegex.test(value);
    }

    /**
     * Vérifie que `n` est strictement un numbre
     * @param {any} n 
     * @returns {number}
     */
    static isInteger(n) {
        if (n == null || ("" + n).trim() === "" || ~~n !== +n)
            return false;
        return true;
    }

    /**
     *
     * @param {String} value
     */
    static encode(value) {
        return Buffer.from(value, 'utf8').toString('base64');
    }

    /**
     *
     * @param {String} value
     */
    static decode(value) {
        return Buffer.from(value, 'base64').toString('utf8');
    }

    /**
     *
     * @param request
     * @param err l'erreur qu'on retourne lorque les informations recus sont mal construites
     * @returns {string[]} retorune un tableau de la forme `[username, password]`
     */
    static getBasicAuthCredentials(request, err) {
        const authorization = request.headers["authorization"];
        if (authorization == undefined)
            throw err;

        const base64 = authorization.split(" ")[1];

        if (base64 == undefined)
            throw err;

        const credentials = Buffer.from(base64, "base64").toString('utf8');
        const index = credentials.indexOf(":");

        if (index == -1)
            throw err;

        return [
            credentials.substr(0, index),
            credentials.substr(index + 1)
        ];
    }

    /**
     * Vérifie que la clé de l'api contenue dans la request est correcte
     * @param {*} request la request
     * @param {*} key la clé de notre api
     * @param {*} err l'objet à retourner en cas d'erreur
     */
    static verifyApiKey(request, key, err) {
        const actualKey = request.headers["X-API-KEY"];
        if (actualKey != key)
            throw err;
    }

}

module.exports = Utils;